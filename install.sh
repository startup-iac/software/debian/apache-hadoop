#!/bin/bash

HADOOP_VERSION=${HADOOP_VERSION:-3.3.6}
HADOOP_HOME="/opt/hadoop"
HADOOP_CONFIG="etc/hadoop"
HADOOP_LOGS="/var/log/hadoop"
HADOOP_LIBS_FILE="/etc/ld.so.conf.d/hadoop.conf"

HDFS_NAMENODE_DIR="/var/lib/hadoop/data/nameNode"
HDFS_DATANODE_DIR="/var/lib/hadoop/data/dataNode"

# Set environment
echo "export HADOOP_HOME=$HADOOP_HOME" | sudo tee -a $ENVIRONMENT_FILE
echo "export PDSH_RCMD_TYPE=ssh" | sudo tee -a $ENVIRONMENT_FILE
echo "export HADOOP_CONF_DIR=$HADOOP_HOME/$HADOOP_CONFIG" | sudo tee -a $ENVIRONMENT_FILE
echo "export HADOOP_LOG_DIR=$HADOOP_LOGS" | sudo tee -a $ENVIRONMENT_FILE
echo "$HADOOP_HOME/lib/native" | sudo tee -a $HADOOP_LIBS_FILE
source /etc/profile

# Install Hadoop
# ------------------------------------------------------------------------------------------------------------------------------------------
# Create directories for Hadoop and set permissions
sudo mkdir -p $HADOOP_HOME
sudo mkdir -p $HADOOP_LOGS
sudo mkdir -p $HDFS_NAMENODE_DIR
sudo mkdir -p $HDFS_DATANODE_DIR

sudo chown -R $USER $HADOOP_HOME
sudo chown -R $USER $HADOOP_LOGS
sudo chown -R $USER $HDFS_NAMENODE_DIR
sudo chown -R $USER $HDFS_DATANODE_DIR

wget https://dlcdn.apache.org/hadoop/common/hadoop-$HADOOP_VERSION/hadoop-$HADOOP_VERSION.tar.gz -O /tmp/hadoop.tar.gz
#wget https://dlcdn.apache.org/hadoop/common/hadoop-$HADOOP_VERSION/hadoop-$HADOOP_VERSION-aarch64.tar.gz -O /tmp/hadoop.tar.gz
#wget https://dlcdn.apache.org/hadoop/common/hadoop-$HADOOP_VERSION/hadoop-$HADOOP_VERSION-aarch64.tar.gz.sha512 -O /tmp/hadoop.tar.gz.sha512
    

tar -xzvf /tmp/hadoop.tar.gz -C $HADOOP_HOME --strip-components=1

#sudo chown -R $HADOOP_USER:$HADOOP_GROUP $HADOOP_HOME
